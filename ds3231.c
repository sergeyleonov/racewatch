#include "ds3231.h"

#include <stm32f10x_gpio.h>
#include <stm32f10x_rcc.h>
#include <stm32f10x_i2c.h>
#include <stm32f10x_exti.h>
#include <stm32f10x_usart.h>
#include <platform_config.h>
#include <delay.h>

#define DS3231_addr		0xD0 // I2C 7-bit slave address (0x68) shifted for 1 bit to the left
#define DS3231_seconds	0x00 // DS3231 seconds address
#define DS3231_control	0x0E // DS3231 control register address
#define DS3231_tmp_MSB	0x11 // DS3231 temperature MSB
#define DS3231_tmp_LSB	0x12 // DS3231 temperature MSB

#define RS1 3 // RS1 bit of the Control register
#define RS2 4 // RS2 bit of the Control register

#define MAX_REPEATS 20000

__IO uint32_t tm_ready = 0;

// All DS3231 registers
typedef struct {
	uint8_t seconds;
	uint8_t minutes;
	uint8_t hours;
	uint8_t day;
	uint8_t date;
	uint8_t month;
	uint8_t year;
	uint8_t alarm1_seconds;
	uint8_t alarm1_minutes;
	uint8_t alarm1_hours;
	uint8_t alarm1_day;
	uint8_t alarm1_date;
	uint8_t alarm2_minutes;
	uint8_t alarm2_hours;
	uint8_t alarm2_day;
	uint8_t alarm2_date;
	uint8_t control;
	uint8_t status;
	uint8_t aging;
	uint8_t msb_temp;
	uint8_t lsb_temp;
} DS3231_registers_TypeDef;

void EXTI15_10_IRQHandler(void) {
	if (EXTI_GetITStatus(EXTI_Line13) != RESET) {
		EXTI_ClearITPendingBit(EXTI_Line13);

		GPIOB->ODR ^= LED1A_PIN; // Toggle LED

		tm_ready = 1;
	}
}

uint32_t DS3231_ReadDateRAW(DS3231_date_TypeDef* date) {
	uint32_t i;
	uint32_t counter;
	uint8_t *buffer = (uint8_t*)date;

	I2C_AcknowledgeConfig(I2C2, ENABLE); // Enable I2C acknowledge

	counter = MAX_REPEATS;
	I2C_GenerateSTART(I2C2, ENABLE); // Send START condition
	while (!I2C_CheckEvent(I2C2, I2C_EVENT_MASTER_MODE_SELECT)) // Wait for EV5
		if (!counter--) return 0;

	counter = MAX_REPEATS;
	I2C_Send7bitAddress(I2C2, DS3231_addr, I2C_Direction_Transmitter); // Send DS3231 slave address
	while (!I2C_CheckEvent(I2C2, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED)) // Wait for EV6
		if (!counter--) return 0;

	counter = MAX_REPEATS;
	I2C_SendData(I2C2, DS3231_seconds); // Send DS3231 seconds register address
	while (!I2C_CheckEvent(I2C2, I2C_EVENT_MASTER_BYTE_TRANSMITTED)) // Wait for EV8
		if (!counter--) return 0;

	counter = MAX_REPEATS;
	I2C_GenerateSTART(I2C2, ENABLE); // Send repeated START condition (aka Re-START)
	while (!I2C_CheckEvent(I2C2, I2C_EVENT_MASTER_MODE_SELECT)) // Wait for EV5
		if (!counter--) return 0;

	counter = MAX_REPEATS;
	I2C_Send7bitAddress(I2C2, DS3231_addr, I2C_Direction_Receiver); // Send DS3231 slave address for READ
	while (!I2C_CheckEvent(I2C2, I2C_EVENT_MASTER_RECEIVER_MODE_SELECTED)) // Wait for EV6
		if (!counter--) return 0;

	for (i = 0; i < 6; i++) {
		counter = MAX_REPEATS;
		while (!I2C_CheckEvent(I2C2, I2C_EVENT_MASTER_BYTE_RECEIVED)) // Wait for EV7 (Byte received from slave)
			if (!counter--) return 0;
		buffer[i] = I2C_ReceiveData(I2C2); // Receive byte
	}

	I2C_AcknowledgeConfig(I2C2, DISABLE); // Disable I2C acknowledgement
	I2C_GenerateSTOP(I2C2, ENABLE); // Send STOP condition

	counter = MAX_REPEATS;
	while (!I2C_CheckEvent(I2C2, I2C_EVENT_MASTER_BYTE_RECEIVED)) // Wait for EV7 (Byte received from slave)
		if (!counter--) return 0;
	buffer[i] = I2C_ReceiveData(I2C2); // Receive last byte
	
	return 1;
}

uint32_t DS3231_WriteDateRAW(DS3231_date_TypeDef* date) {
	uint32_t i;
	uint32_t counter;
	uint8_t *buffer = (uint8_t *)date;

	I2C_AcknowledgeConfig(I2C2, ENABLE); // Enable I2C acknowledge

	counter = MAX_REPEATS;
	I2C_GenerateSTART(I2C2, ENABLE); // Send START condition
	while (!I2C_CheckEvent(I2C2, I2C_EVENT_MASTER_MODE_SELECT)) // Wait for EV5
		if (!counter--) return 0;

	counter = MAX_REPEATS;
	I2C_Send7bitAddress(I2C2, DS3231_addr, I2C_Direction_Transmitter); // Send DS3231 slave address
	while (!I2C_CheckEvent(I2C2, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED)) // Wait for EV6
		if (!counter--) return 0;

	counter = MAX_REPEATS;
	I2C_SendData(I2C2, DS3231_seconds); // Send DS3231 seconds register address
	while (!I2C_CheckEvent(I2C2, I2C_EVENT_MASTER_BYTE_TRANSMITTED)) // Wait for EV8
		if (!counter--) return 0;

	for (i = 0; i < 7; i++) {
		counter = MAX_REPEATS;
		I2C_SendData(I2C2, buffer[i]); // Send DS3231 seconds register address
		while (!I2C_CheckEvent(I2C2, I2C_EVENT_MASTER_BYTE_TRANSMITTED)) // Wait for EV8
			if (!counter--) return 0;
	}

	I2C_GenerateSTOP(I2C2, ENABLE);
	
	return 1;
}

void DS3231_ReadDate(HRF_date_TypeDef* hrf_date) {
	DS3231_date_TypeDef raw_date;

	DS3231_ReadDateRAW(&raw_date);

	hrf_date->Seconds = (raw_date.seconds >> 4) * 10 + (raw_date.seconds & 0x0f);
	hrf_date->Minutes = (raw_date.minutes >> 4) * 10 + (raw_date.minutes & 0x0f);
	hrf_date->Hours = (raw_date.hours >> 4) * 10 + (raw_date.hours & 0x0f);
	hrf_date->Day = (raw_date.date >> 4) * 10 + (raw_date.date & 0x0f);
	hrf_date->Month = (raw_date.month >> 4) * 10 + (raw_date.month & 0x0f);
	hrf_date->Year = (raw_date.year >> 4) * 10 + (raw_date.year & 0x0f) + 2000;
	hrf_date->DOW = raw_date.day_of_week;
}

void DS3231_DateToTimeStr(DS3231_date_TypeDef* raw_date, char *str) {
	*str++ = (raw_date->hours >> 4) + '0';
	*str++ = (raw_date->hours & 0x0f) + '0';
	*str++ = ':';
	*str++ = (raw_date->minutes >> 4) + '0';
	*str++ = (raw_date->minutes & 0x0f) + '0';
	*str++ = ':';
	*str++ = (raw_date->seconds >> 4) + '0';
	*str++ = (raw_date->seconds & 0x0f) + '0';
	*str++ = 0;
}

void DS3231_DateToDateStr(DS3231_date_TypeDef* raw_date, char *str) {
	*str++ = (raw_date->date >> 4) + '0';
	*str++ = (raw_date->date & 0x0f) + '0';
	*str++ = '.';
	*str++ = (raw_date->month >> 4) + '0';
	*str++ = (raw_date->month & 0x0f) + '0';
	*str++ = '.';
	*str++ = '2'; *str++ = '0';
	*str++ = (raw_date->year >> 4) + '0';
	*str++ = (raw_date->year & 0x0f) + '0';
	*str++ = 0;
}

int8_t DS3231_ReadTemp(void) {
	uint32_t counter;
	int8_t temperature;
	
	I2C_AcknowledgeConfig(I2C2, ENABLE); // Enable I2C acknowledge

	counter = MAX_REPEATS;
	I2C_GenerateSTART(I2C2, ENABLE); // Send START condition
	while (!I2C_CheckEvent(I2C2, I2C_EVENT_MASTER_MODE_SELECT)) // Wait for EV5
		if (!counter--) return 0;

	counter = MAX_REPEATS;
	I2C_Send7bitAddress(I2C2, DS3231_addr, I2C_Direction_Transmitter); // Send DS3231 slave address
	while (!I2C_CheckEvent(I2C2, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED)) // Wait for EV6
		if (!counter--) return 0;

	counter = MAX_REPEATS;
	I2C_SendData(I2C2, DS3231_tmp_MSB); // Send DS3231 temperature MSB register address
	while (!I2C_CheckEvent(I2C2, I2C_EVENT_MASTER_BYTE_TRANSMITTED)) // Wait for EV8
		if (!counter--) return 0;

	counter = MAX_REPEATS;
	I2C_GenerateSTART(I2C2, ENABLE); // Send repeated START condition (aka Re-START)
	while (!I2C_CheckEvent(I2C2, I2C_EVENT_MASTER_MODE_SELECT)) // Wait for EV5
		if (!counter--) return 0;

	counter = MAX_REPEATS;
	I2C_Send7bitAddress(I2C2, DS3231_addr, I2C_Direction_Receiver); // Send DS3231 slave address for READ
	while (!I2C_CheckEvent(I2C2, I2C_EVENT_MASTER_RECEIVER_MODE_SELECTED)) // Wait for EV6
		if (!counter--) return 0;

	counter = MAX_REPEATS;
	while (!I2C_CheckEvent(I2C2, I2C_EVENT_MASTER_BYTE_RECEIVED)) // Wait for EV7 (Byte received from slave)
		if (!counter--) return 0;
	temperature = I2C_ReceiveData(I2C2); // Receive temperature MSB

	I2C_AcknowledgeConfig(I2C2, DISABLE); // Disable I2C acknowledgement

	counter = MAX_REPEATS;
	I2C_GenerateSTOP(I2C2, ENABLE); // Send STOP condition
	while (!I2C_CheckEvent(I2C2, I2C_EVENT_MASTER_BYTE_RECEIVED)) // Wait for EV7 (Byte received from slave)
		if (!counter--) return 0;

	return temperature;
}

void DS3231_InitExti()
{
	GPIO_InitTypeDef GPIO_InitStructure;
	EXTI_InitTypeDef EXTIInit;
	NVIC_InitTypeDef NVICInit;
	
	// EXTI pin
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);
	GPIO_StructInit(&GPIO_InitStructure); // All | 2MHz | IN_FLOATING
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_13;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
	GPIO_Init(GPIOC, &GPIO_InitStructure);
	
	GPIO_EXTILineConfig(GPIO_PortSourceGPIOC, GPIO_PinSource13);

	// Configure EXTI line13
	EXTI_StructInit(&EXTIInit);
	EXTIInit.EXTI_Line = EXTI_Line13;
	EXTIInit.EXTI_LineCmd = ENABLE;
	EXTIInit.EXTI_Mode = EXTI_Mode_Interrupt; // Generate IRQ
	EXTIInit.EXTI_Trigger = EXTI_Trigger_Rising;
	EXTI_Init(&EXTIInit);

	// Configure EXTI1 interrupt
	NVICInit.NVIC_IRQChannel = EXTI15_10_IRQn; // EXTI_Line13 -> EXTI15_10_IRQn
	NVICInit.NVIC_IRQChannelCmd = ENABLE;
	NVICInit.NVIC_IRQChannelPreemptionPriority = 0x01;
	NVICInit.NVIC_IRQChannelSubPriority = 0x01;
	NVIC_Init(&NVICInit);
}

int32_t DS3231_Init(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	I2C_InitTypeDef I2CInit;
	
	uint32_t counter;
	int32_t temperature;

	// Init I2C
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
	GPIO_StructInit(&GPIO_InitStructure); // All | 2MHz | IN_FLOATING
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10 | GPIO_Pin_11;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_OD; // PP onboard?
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOB, &GPIO_InitStructure);

	RCC_APB1PeriphClockCmd(RCC_APB1Periph_I2C2, ENABLE); // Enable I2C clock
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);

	I2C_DeInit(I2C2); // I2C reset to initial state
	I2CInit.I2C_Mode = I2C_Mode_I2C; // I2C mode is I2C
	I2CInit.I2C_DutyCycle = I2C_DutyCycle_2; // I2C fast mode duty cycle
	I2CInit.I2C_OwnAddress1 = 1; // This device address (7-bit or 10-bit)
	I2CInit.I2C_Ack = I2C_Ack_Enable; // Acknowledgement enable
	I2CInit.I2C_AcknowledgedAddress = I2C_AcknowledgedAddress_7bit; // choose 7-bit address for acknowledgement
	I2CInit.I2C_ClockSpeed = 50000; // 400kHz ?
	I2C_Init(I2C2, &I2CInit); // Configure I2C
	I2C_Cmd(I2C2, ENABLE); // Enable I2C

	counter = MAX_REPEATS;
	while (I2C_GetFlagStatus(I2C2, I2C_FLAG_BUSY)) // Wait until I2C free
		if (!counter--) return -1;
	
	delay_ms(100); // ?
	
	// Check connection to DS3231
	counter = MAX_REPEATS;
	I2C_GenerateSTART(I2C2, ENABLE); // Send START condition
	while (!I2C_CheckEvent(I2C2, I2C_EVENT_MASTER_MODE_SELECT)) // Wait for EV5
		if (!counter--) return -2;
	counter = MAX_REPEATS;
	I2C_Send7bitAddress(I2C2, DS3231_addr, I2C_Direction_Transmitter); // Send DS3231 slave address
	while (!I2C_CheckEvent(I2C2, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED)) // Wait for EV6
		if (!counter--) return -3;
	I2C_GenerateSTOP(I2C2, ENABLE);

	// Wait for 250 ms for DS3231 startup
	delay_ms(250); // TODO: Is it necessary?

	// DS3231 init
	counter = MAX_REPEATS;
	I2C_GenerateSTART(I2C2, ENABLE);
	while (!I2C_CheckEvent(I2C2, I2C_EVENT_MASTER_MODE_SELECT)) // Wait for EV5
		if (!counter--) return -4;
	counter = MAX_REPEATS;
	I2C_Send7bitAddress(I2C2, DS3231_addr, I2C_Direction_Transmitter); // Send DS3231 slave address
	while (!I2C_CheckEvent(I2C2, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED)) // Wait for EV6
		if (!counter--) return -5;
	counter = MAX_REPEATS;
	I2C_SendData(I2C2, DS3231_control); // Send DS3231 control register address
	while (!I2C_CheckEvent(I2C2, I2C_EVENT_MASTER_BYTE_TRANSMITTED)) // Wait for EV8
		if (!counter--) return -6;
	
	counter = MAX_REPEATS;
	// 00 -> 1Hz, 01 -> 1024 Hz, 10 -> 4096 Hz, 11 -> 8192 Hz
	I2C_SendData(I2C2, 0 << RS2 | 1 << RS1); // DS3231 EOSC enabled, SQW enabled, SQW set to 1024Hz
	while (!I2C_CheckEvent(I2C2, I2C_EVENT_MASTER_BYTE_TRANSMITTED)) // Wait for EV8
		if (!counter--) return -7;
	counter = MAX_REPEATS;
	I2C_SendData(I2C2, 0x00); // DS3231 clear alarm flags
	while (!I2C_CheckEvent(I2C2, I2C_EVENT_MASTER_BYTE_TRANSMITTED)) // Wait for EV8
		if (!counter--) return -8;
	I2C_GenerateSTOP(I2C2, ENABLE);

	temperature = DS3231_ReadTemp();
	return temperature;
}
