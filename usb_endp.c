/**
  ******************************************************************************
  * @file    USB_Example/usb_endp.c
  * @author  MCD Application Team
  * @version V1.1.0
  * @date    20-September-2012
  * @brief   Endpoint routines
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2012 STMicroelectronics</center></h2>
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "usb_lib.h"
#include "usb_istr.h"
#include "usb_desc.h"

/** @addtogroup STM32F3_Discovery_Peripheral_Examples
  * @{
  */

/** @addtogroup USB_Example
  * @{
  */ 

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
uint8_t Receive_Buffer[9];
extern __IO uint8_t PrevXferComplete;
extern uint8_t Buffer[RPT4_COUNT+1];

/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

/**
  * @brief  EP1 OUT Callback Routine.
  * @param  None
  * @retval None
  */
void EP1_IN_Callback(void)
{
  /* Set the transfer complete token to inform upper layer that the current 
  transfer has been complete */
  PrevXferComplete = 1; 
}

/*******************************************************************************
* Function Name  : EP1_OUT_Callback.
* Description    : EP1 OUT Callback Routine.
* Input          : None.
* Output         : None.
* Return         : None.
*******************************************************************************/
void EP1_OUT_Callback(void)
{
  // Здесь обрабатываются данные, отправленные методом SET_REPORT
  BitAction Led_State;
  
  // Switch status LED
  if (GPIO_ReadInputDataBit(LED_PORT, LED1A_PIN) == Bit_RESET)
  {
    GPIO_SetBits(LED_PORT, LED1A_PIN);
  }
  else
  {
    GPIO_ResetBits(LED_PORT, LED1A_PIN);
  }

  /* Read received data (2 bytes) */
  USB_SIL_Read(EP1_OUT, Receive_Buffer);

  if (Receive_Buffer[1] == 0)
  {
    Led_State = Bit_RESET;
  }
  else
  {
    Led_State = Bit_SET;
  }

  switch (Receive_Buffer[0])
  {
    case 1: /* Led 1 */
      if (Led_State != Bit_RESET)
      {
        GPIO_SetBits(LED_PORT, LED1A_PIN);
      }
      else
      {
        GPIO_ResetBits(LED_PORT, LED1A_PIN);
      }
      break;
      
    case 2: /* Led 2 */
      if (Led_State != Bit_RESET)
      {
        GPIO_SetBits(LED_PORT, LED1A_PIN);
      }
      else
      {
        GPIO_ResetBits(LED_PORT, LED1A_PIN);
      }
      break;
      
    case 3: /* Led 1&2 */
      Buffer[4] = Receive_Buffer[1];
      break;
  }

  SetEPRxStatus(ENDP1, EP_RX_VALID);
}

/**
  * @}
  */ 

/**
  * @}
  */ 

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/ 

