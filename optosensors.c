#include "optosensors.h"
#include "platform_config.h"
#include "delay.h"
#include "clocktimer.h"
#include "systemmode.h"
#include "led_display.h"

#define SENSORS_NUM 7
#define MEASURES_NUM 12
#define ARRAYSIZE (SENSORS_NUM * MEASURES_NUM)

static uint16_t ADC_buffer[ARRAYSIZE]; // static -> hide from other modules
//uint32_t __IO DMA_Complete = 0;
//uint16_t results_cur[SENSORS_NUM];
static uint16_t results_est[SENSORS_NUM]; // static -> hide from other modules
//int8_t results_switched[SENSORS_NUM] = {0};

__IO float internal_temp;
//__IO uint32_t MeasuresCounter;
__IO uint32_t ResultTime = 0;
uint32_t ResultSpeedTime = 0;
__IO uint32_t ResultSpeed = 0;

extern uint32_t SystemMode;

void Opto_ADCInit(void);
void Opto_DMAInit(void);

void __inline Opto_CalculateResults(uint16_t *results)
{
	uint32_t sensor;
	uint32_t measure;
	
	for (sensor = 0; sensor < SENSORS_NUM; sensor++)
	{
		uint32_t sum = 0;
		for (measure = 0; measure < MEASURES_NUM; measure++)
		{
			sum += ADC_buffer[measure * SENSORS_NUM + sensor];
		}
		results[sensor] = sum / MEASURES_NUM;
	}
}

/*void __inline Opto_CalculateCurResults()
{
	Opto_CalculateResults(results_cur);
}*/

void Opto_RefreshEstResults() {
	uint32_t sensor;
	uint16_t results[SENSORS_NUM];
	uint32_t parameter;
	
	Opto_CalculateResults(results); // TODO?
	for (sensor = 0; sensor < SENSORS_NUM; sensor++)
	{
		uint32_t current = results[sensor];
		uint32_t estimated = results_est[sensor];
		int32_t diff = current - estimated;
		if (diff > 0)
		{
			if (estimated == 0) {
				results_est[sensor] = current; // Avoids dividing by 0 and startup misfire
				continue;
			}
			parameter = (diff << 4) / estimated;
			if (parameter > 2)
			{
				if (sensor == 3) {
					if (SystemMode != SS_Started) {
						ClockTimer_ResetStart();
						ResultSpeedTime = ClockTimer_Get();
						ResultTime = 0;
						ResultSpeed = 0;
						SystemMode = SS_Started;
					}
					//results_switched[sensor] = 3;
				}
				else if (sensor == 4) {
					if (SystemMode == SS_Started) {
						ResultTime = ClockTimer_Stop();
						ResultSpeed = 3600.0f / 1000.0f * 10.0f * 1024.0f / (ResultTime - ResultSpeedTime); // 1000m per 3600 seconds
						SystemMode = SS_Stopped;
					}
					//results_switched[sensor] = 4;
				}
				else if (sensor == 2) {
					if (SystemMode == SS_Started) {
						ResultSpeedTime = ClockTimer_Get();
					}
					//results_switched[sensor] = 4;
				}
				else {
					//results_switched[sensor] = 1;
				}
			}
			else
			{
				//results_switched[sensor] = 0;
			}
		}
		results_est[sensor] = (4 * results_est[sensor] + current) / 5;
	}
	
	// TempSensor = [5]; Vrefint = [6]
	// Replace dividing by 4.3f with mutiplying by 0.23256f => 60 bytes
	internal_temp = (((1.43f / 1.2f * (float)(results_est[6])) - (float)(results_est[5])) * 0.23256f + 25.0f);
}

void Opto_Start(void)
{
	//Enable DMA1 Channel transfer
	DMA_Cmd(DMA1_Channel1, ENABLE);
	//Start ADC1 Software Conversion
	ADC_SoftwareStartConvCmd(ADC1, ENABLE);
}

void Opto_Init()
{
	Opto_ADCInit();
	Opto_DMAInit();
}

void Opto_ADCInit(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	ADC_InitTypeDef ADC_InitStructure;
	
	//Enable ADC1 and GPIOA
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
	// Configure ADC pins (PA0 -> Channel 0 to PA3 -> Channel 3) as analog inputs
	GPIO_StructInit(&GPIO_InitStructure); // All | 2MHz | IN_FLOATING
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3 | GPIO_Pin_4;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
	
	//ADC1 configuration
	ADC_StructInit(&ADC_InitStructure);
	ADC_InitStructure.ADC_Mode = ADC_Mode_Independent;
	ADC_InitStructure.ADC_ScanConvMode = ENABLE;
	ADC_InitStructure.ADC_ContinuousConvMode = ENABLE;
	ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_None;
	ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
	ADC_InitStructure.ADC_NbrOfChannel = SENSORS_NUM;
	ADC_Init(ADC1, &ADC_InitStructure);
	//wake up temperature sensor
	ADC_TempSensorVrefintCmd(ENABLE);
	//configure each channel
	ADC_RegularChannelConfig(ADC1, ADC_Channel_0, 1, ADC_SampleTime_239Cycles5);
	ADC_RegularChannelConfig(ADC1, ADC_Channel_1, 2, ADC_SampleTime_239Cycles5);
	ADC_RegularChannelConfig(ADC1, ADC_Channel_2, 3, ADC_SampleTime_239Cycles5);
	ADC_RegularChannelConfig(ADC1, ADC_Channel_3, 4, ADC_SampleTime_239Cycles5);
	ADC_RegularChannelConfig(ADC1, ADC_Channel_4, 5, ADC_SampleTime_239Cycles5);
	ADC_RegularChannelConfig(ADC1, ADC_Channel_TempSensor, 6, ADC_SampleTime_41Cycles5);
	ADC_RegularChannelConfig(ADC1, ADC_Channel_Vrefint, 7, ADC_SampleTime_41Cycles5);

	ADC_Cmd(ADC1, ENABLE);
	ADC_DMACmd(ADC1, ENABLE);
	
	//Enable ADC1 reset calibration register
	ADC_ResetCalibration(ADC1);
	//Check the end of ADC1 reset calibration register
	while(ADC_GetResetCalibrationStatus(ADC1));
	//Start ADC1 calibration
	ADC_StartCalibration(ADC1);
	//Check the end of ADC1 calibration
	while(ADC_GetCalibrationStatus(ADC1));
}

void Opto_DMAInit(void)
{
	DMA_InitTypeDef DMA_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;

	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);
	DMA_DeInit(DMA1_Channel1);
	DMA_StructInit(&DMA_InitStructure);
	
	DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;
	DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;
	DMA_InitStructure.DMA_Priority = DMA_Priority_High;
	DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;
	DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord;
	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralSRC;
	DMA_InitStructure.DMA_BufferSize = ARRAYSIZE;
	DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t)&(ADC1->DR); // Should be 0x4001244C
	DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t)ADC_buffer;
	DMA_Init(DMA1_Channel1, &DMA_InitStructure);
	// Enable DMA1 Channel Transfer Complete interrupt
	DMA_ITConfig(DMA1_Channel1, DMA_IT_TC, ENABLE);
	DMA_Cmd(DMA1_Channel1, ENABLE); //Enable the DMA1 - Channel1
	
	//Enable DMA1 IRQ Channel
	NVIC_InitStructure.NVIC_IRQChannel = DMA1_Channel1_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
}
