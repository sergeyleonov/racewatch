#ifndef __DS3231_H
#define __DS3231_H
#include "stm32f10x.h"

// DS3231 date
typedef struct {
	uint8_t seconds;
	uint8_t minutes;
	uint8_t hours;
	uint8_t day_of_week;
	uint8_t date;
	uint8_t month;
	uint8_t year;
} DS3231_date_TypeDef;

// Human Readable Format date
typedef struct {
	uint8_t Seconds;
	uint8_t Minutes;
	uint8_t Hours;
	uint8_t Day;
	uint8_t Month;
	uint16_t Year;
	uint8_t DOW;
} HRF_date_TypeDef;

int32_t DS3231_Init(void);
uint32_t DS3231_ReadDateRAW(DS3231_date_TypeDef *);
void DS3231_ReadDate(HRF_date_TypeDef *);
void DS3231_DateToTimeStr(DS3231_date_TypeDef *, char *);
void DS3231_DateToDateStr(DS3231_date_TypeDef *, char *);

#endif
