#ifndef __CLOCKTIMER_H
#define __CLOCKTIMER_H

#include "stm32f10x.h"

void ClockTimer_Init(void);
void ClockTimer_ResetStart(void);
uint16_t ClockTimer_Get(void);
uint16_t ClockTimer_Stop(void);

#endif
