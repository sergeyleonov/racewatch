#ifndef _HARDWARESPI_H_
#define _HARDWARESPI_H_

#include "stm32f10x.h"
#include "stm32f10x_gpio.h"
#include "stm32f10x_rcc.h"
#include "stm32f10x_spi.h"

#define RF_FLAG_TIMEOUT 10000

// SPI_Freq = (SYSCLK / (AHB_Presc * ABP1_Presc)) / SPI_Presc
#define RF24_SPI_Prescaler	SPI_BaudRatePrescaler_32 // 64 -> optimum performance
#define RF24_RCC_Periph		RCC_APB2Periph_SPI1
//#define RF24_RCC_Periph	RCC_APB1Periph_SPI1 // STM32F3
#define RF24_SPIx			SPI1
#define RF24_SPI_GPIO		GPIOA
#define RF24_SPI_APB2_GPIO	RCC_APB2Periph_GPIOA
//#define RF24_SPI_AHB_GPIO	RCC_AHBPeriph_GPIOB // STM32F3
#define RF24_MOSI_PIN		7
#define RF24_MISO_PIN		6
#define RF24_SCK_PIN		5

#define RF24_GPIO			GPIOB
#define RF24_APB2_GPIO		RCC_APB2Periph_GPIOB
//#define RF24_AHB_GPIO		RCC_AHBPeriph_GPIOD // STM32F3
#define RF24_CE_PIN			10
#define RF24_CS_PIN			11

/**
 * @brief Defines the possible SPI communication speeds.
 */
typedef enum SPIFrequency {
    SPI_18MHZ       = 0, /**< 18 MHz */
    SPI_9MHZ        = 1, /**< 9 MHz */
    SPI_4_5MHZ      = 2, /**< 4.5 MHz */
    SPI_2_25MHZ     = 3, /**< 2.25 MHz */
    SPI_1_125MHZ    = 4, /**< 1.125 MHz -> 64*/
    SPI_562_500KHZ  = 5, /**< 562.500 KHz */
    SPI_281_250KHZ  = 6, /**< 281.250 KHz */
    SPI_140_625KHZ  = 7, /**< 140.625 KHz */
} SPIFrequency;

    /*
     * Set up/tear down
     */

    /**
     * @brief Turn on a SPI port and set its GPIO pin modes for use as master.
     *
     * SPI port is enabled in full duplex mode, with software slave management.
     *
     * @param frequency Communication frequency
     * @param bitOrder Either LSBFIRST (little-endian) or MSBFIRST (big-endian)
     */
    void HardwareSPI_begin(void);

    /*
     * I/O
     */

    /**
     * @brief Read length bytes, storing them into buffer.
     * @param buffer Buffer to store received bytes into.
     * @param length Number of bytes to store in buffer.  This
     *               function will block until the desired number of
     *               bytes have been read.
     */
    void HardwareSPI_read(uint8_t *buffer, uint32_t length);

    /**
     * @brief Transmit multiple bytes.
     * @param buffer Bytes to transmit.
     * @param length Number of bytes in buffer to transmit.
     */
    void HardwareSPI_write(const uint8_t *buffer, uint32_t length);

    /**
     * @brief Transmit a byte, then return the next unread byte.
     *
     * This function transmits before receiving.
     *
     * @param data Byte to transmit.
     * @return Next unread byte.
     */
    uint8_t HardwareSPI_transfer(uint8_t data);

#endif

