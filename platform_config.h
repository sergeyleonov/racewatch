/**
  ******************************************************************************
  * @file    USB_Example/platform_config.h 
  * @author  MCD Application Team
  * @version V1.1.0
  * @date    20-September-2012
  * @brief   Evaluation board specific configuration file.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2012 STMicroelectronics</center></h2>
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __PLATFORM_CONFIG_H
#define __PLATFORM_CONFIG_H

/* Includes ------------------------------------------------------------------*/

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/


#define USB_INT_DEFAULT// For Default Interrupt Mode
//#define USB_INT_REMAP // For Remapping Interrupt Mode

#define ID1		(0x1FFFF7E8)
#define ID2		(0x1FFFF7EC)
#define ID3		(0x1FFFF7F0)

#define RCC_APB2Periph_GPIO_BUTTON	RCC_APB2Periph_GPIOB
#define BTN1_PORT					GPIOB
#define BTN1_PIN					GPIO_Pin_8

#define RCC_APB2Periph_GPIO_LED		RCC_APB2Periph_GPIOB
#define LED_PORT					GPIOB
#define LED1A_PIN					GPIO_Pin_1

#if !defined (USB_INT_DEFAULT) && !defined (USB_INT_REMAP)
  #error "Missing define Please Define Your Interrupt Mode By UnComment Line in platform_config.h File"
#endif

#define USB_DISCONNECT					GPIOB
#define RCC_APB2Periph_GPIO_DISCONNECT	RCC_APB2Periph_GPIOB
#define USB_DISCONNECT_PIN				GPIO_Pin_9

#endif /* __PLATFORM_CONFIG_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
