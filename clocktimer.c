#include "clocktimer.h"
#include "stm32f10x_tim.h"

void ClockTimer_ResetStart()
{
	TIM_Cmd(TIM1, DISABLE);
	TIM1->CNT = 0;
	TIM_Cmd(TIM1, ENABLE);
}

uint16_t ClockTimer_Get()
{
	uint16_t cnt = 0;
	cnt = TIM1->CNT;
	return cnt;
}

uint16_t ClockTimer_Stop()
{
	uint16_t cnt = 0;
	TIM_Cmd(TIM1, DISABLE);
	cnt = TIM1->CNT;
	return cnt;
}

void ClockTimer_Init()
{
	GPIO_InitTypeDef GPIO_InitStructure;
	TIM_ICInitTypeDef TIM_ICInitStructure;
	
	/* TIM1 clock enable */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM1, ENABLE);

	/* GPIO clock enable */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);

	/* GPIO Configuration: PA9(TIM1 CH2)  */
	GPIO_StructInit(&GPIO_InitStructure); // All | 2MHz | IN_FLOATING
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	/* TIM1 Input trigger configuration: External Trigger connected to TI2 */
	TIM_ICStructInit(&TIM_ICInitStructure);
	TIM_ICInitStructure.TIM_Channel = TIM_Channel_2;
	TIM_ICInitStructure.TIM_ICPolarity = TIM_ICPolarity_Rising;
	TIM_ICInitStructure.TIM_ICSelection = TIM_ICSelection_DirectTI;
	TIM_ICInitStructure.TIM_ICPrescaler = TIM_ICPSC_DIV1;
	TIM_ICInitStructure.TIM_ICFilter = 0;
	TIM_ICInit(TIM1, &TIM_ICInitStructure);

	TIM_TIxExternalClockConfig(TIM1, TIM_TS_TI2FP2, TIM_ICPolarity_Rising, 0x04); // 0100: fSAMPLING=fDTS/2, N=6
	TIM_SelectSlaveMode(TIM1, TIM_SlaveMode_External1);

	TIM_Cmd(TIM1, ENABLE);
}
