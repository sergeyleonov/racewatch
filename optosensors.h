#ifndef __OPTOSENSORS_H
#define __OPTOSENSORS_H

#include "stm32f10x.h"

void Opto_Init(void);
void Opto_Start(void);
void Opto_CalculateResults(uint16_t *);
void Opto_CalculateCurResults(void);
void Opto_RefreshEstResults(void);

#endif
