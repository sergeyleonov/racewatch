#ifndef __PAYLOAD_H__
#define __PAYLOAD_H__

#include "stm32f10x.h"

struct Payload_struct {
	uint32_t answerType;
	union answer {
		uint32_t time;
		float vbat;
	} data;
};

typedef struct Payload_struct Payload;

enum AnswerTypes {
  CURRENT_TIME_ANSWER = 0,
  SPEED_TIME_ANSWER,
  STOP_TIME_ANSWER,
  VBAT_ANSWER
};

uint8_t addresses[][6] = {"1Node","2Node"};

float measure;

#endif
