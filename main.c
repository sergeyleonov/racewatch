#include "stm32f10x.h"
#include "stm32f10x_it.h"

#include "hw_config.h"
#include "delay.h"
#include "led_display.h"
#include "systemmode.h"
#include "RF24.h"
#include "payload.h"
#include "sensorsint.h"

const uint32_t INIT_PAUSE = 1000;
const uint32_t DISPLAY_PAUSE = 2500;

extern uint16_t execTime;
extern uint32_t SysTickCounter;
extern uint32_t ResultTime;
extern uint32_t ResultSpeed;
extern uint32_t StartTime;

uint32_t timeDiff = 0;
uint32_t remoteSysTime = 0;
uint32_t remoteStopTime = 0;
uint32_t remoteVBat = 0;

__IO char compile_time[] = __TIME__;
__IO char compile_date[] = __DATE__;
__IO uint32_t SystemMode = SS_Stopped; // TODO

Payload payload;

enum Views {
	SPEED_VIEW,
	TIME_VIEW
};

void MainLoop() {
	uint32_t lastSwitchTime = SysTickCounter;
	uint32_t view = TIME_VIEW;
	
	char progressSymbol = 'a';
	
	while(1) {
		if (SystemMode == SS_Stopped) {
			if (SysTickCounter - lastSwitchTime > DISPLAY_PAUSE) {
				lastSwitchTime = SysTickCounter;
				if (view == TIME_VIEW) {
					view = SPEED_VIEW;
					
					//ledDisplay_SetMsecs(ResultTime); // TODO: revert
					ledDisplay_SetSpeed(timeDiff * 10); // TODO: remove multiplier
				}
				else if (view == SPEED_VIEW) {
					view = TIME_VIEW;
					
					ledDisplay_SetSpeed(remoteVBat);
					/*ledDisplay_SetSpeed(ResultSpeed * 10); // TODO: remove multiplier
					ledDisplay_SetChar(progressSymbol, 0);
					progressSymbol++;
					if (progressSymbol > 'f')
						progressSymbol = 'a';*/
				}
			}
		}
		
		if (RF24_available()) {
			uint32_t msecTime = 0;
			
			RF24_read(&payload, sizeof(Payload));  // Get the payload TODO: clear receive buffer

			msecTime = (uint32_t)(((uint64_t)payload.data.time * 125L) / 128L);
			
			if (payload.answerType == CURRENT_TIME_ANSWER) {
				if (remoteSysTime == 0) // The first reading
					SysTickCounter = msecTime;
				remoteSysTime = msecTime;
				timeDiff = (remoteSysTime > SysTickCounter)? (remoteSysTime - SysTickCounter): (SysTickCounter - remoteSysTime); // Signed!
			}
			else if (payload.answerType == STOP_TIME_ANSWER) {
				remoteStopTime = msecTime;
			}
			else if (payload.answerType == VBAT_ANSWER) {
				remoteVBat = payload.data.vbat * 1000; // in mV
			}
		}
	}
}

void InitRF() {
	RF24_begin();

	// Set the PA Level low to prevent power supply related issues since this is a
	// getting_started sketch, and the likelihood of close proximity of the devices. RF24_PA_MAX is default.
	RF24_setPALevel(RF24_PA_LOW); // TODO: Set max. power
	
	// Set payload size prior to open the pipes
	RF24_setPayloadSize(sizeof(Payload));
	
	// Open writing and reading pipes, with opposite addresses

	RF24_openWritingPipe_name(addresses[0]);
	RF24_openReadingPipe_name(1, addresses[1]);

	// Start the listening for data
	RF24_startListening();
}

int main(void) {
	// Enable debugging in power-save modes
	DBGMCU->CR = DBGMCU_CR_DBG_SLEEP | DBGMCU_CR_DBG_STOP | DBGMCU_CR_DBG_STANDBY;
	
	InitSensors();
	InitSysTick();
	InitDelayTimer();
	InitLedsButtons();
	
	ledDisplay_Init();
	ledDisplay_SetSpeed(8888);
	delay_ms(INIT_PAUSE);
	
	InitRF();
	
	// Endless loop starts here
	MainLoop();

	// Only if the button is pressed
	/*if (GPIO_ReadInputDataBit(BTN1_PORT, BTN1_PIN) == Bit_RESET) {
		// Configure the USB
		USB_Config();
	}
	
	while (1) {
		// USB
		if (bDeviceState == CONFIGURED)
			if (PrevXferComplete)
				RHIDCheckState();
		// EOUSB
	}*/
}

#ifdef	USE_FULL_ASSERT

/**
	* @brief	Reports the name of the source file and the source line number
	*				 where the assert_param error has occurred.
	* @param	file: pointer to the source file name
	* @param	line: assert_param error line source number
	* @retval None
	*/
void assert_failed(uint8_t* file, uint32_t line) {
	/* User can add his own implementation to report the file name and line number,
		 ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

	/* Infinite loop */
	while (1) {
	}
}
#endif
