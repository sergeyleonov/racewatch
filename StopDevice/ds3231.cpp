#include "ds3231.h"

// Convert normal decimal to binary coded decimal
static byte decToBcd(byte val)
{
  return((val/10*16) + (val%10));
}

// Convert binary coded decimal to normal decimal
static byte bcdToDec(byte val)
{
  return((val/16*10) + (val%16));
}

void ds3231TimeSet(Time &time)
{
  // set time and date data to DS3231
  Wire.beginTransmission(DS3231_I2C_ADDRESS);
  Wire.write(0);
  Wire.write(decToBcd(time.second)); // set seconds
  Wire.write(decToBcd(time.minute)); // set minutes
  Wire.write(decToBcd(time.hour)); // set hours
  Wire.write(decToBcd(time.dayOfWeek)); // set day of week
  Wire.write(decToBcd(time.date)); // set date (1 to 31)
  Wire.write(decToBcd(time.month)); // set month
  Wire.write(decToBcd(time.year)); // set year (0 to 99)
  Wire.endTransmission();
}

void ds3231TimeGet(Time &time)
{
  Wire.beginTransmission(DS3231_I2C_ADDRESS);
  Wire.write(0); // set DS3231 register pointer to 00h
  Wire.endTransmission();
  Wire.requestFrom(DS3231_I2C_ADDRESS, (uint8_t)7);
  
  // request seven bytes of data from DS3231 starting from register 00h
  time.second = bcdToDec(Wire.read() & 0x7f);
  time.minute = bcdToDec(Wire.read());
  time.hour = bcdToDec(Wire.read() & 0x3f);
  time.dayOfWeek = bcdToDec(Wire.read());
  time.date = bcdToDec(Wire.read());
  time.month = bcdToDec(Wire.read());
  time.year = bcdToDec(Wire.read());
}

float ds3231TempGet()
{
  Wire.beginTransmission(DS3231_I2C_ADDRESS);
  Wire.write(DS3231_REG_TEMP_MSB);
  Wire.endTransmission();
  Wire.requestFrom(DS3231_I2C_ADDRESS, (uint8_t)2);

  char temp_msb = Wire.read();
  byte temp_lsb = Wire.read() >> 6;
  
  float temp = (float)temp_lsb * 0.25;  // total up,  .00, .25, .50
  if (temp_msb < 0) {
    temp = temp_msb - temp;             // calculate the fract correctly
  } else {
    temp = temp_msb + temp;             // add    25 + .25 = 25.25, and -25 - .25 = -25.25
  }
  return temp;
}

byte ds3231RegRead(byte reg)
{
  Wire.beginTransmission(DS3231_I2C_ADDRESS);
  Wire.write(reg);
  Wire.endTransmission();
  Wire.requestFrom(DS3231_I2C_ADDRESS, (uint8_t)1);

  return Wire.read();
}

void ds3231RegWrite(byte reg, byte val)
{
  Wire.beginTransmission(DS3231_I2C_ADDRESS);
  Wire.write(reg);
  Wire.write(val);
  Wire.endTransmission();
}

// 0 -> 1 Hz, 1 -> 1024 Hz, 2 -> 4096 Hz, 3 -> 8192 Hz
void ds3231RateSet(byte rate)
{
  byte control = ds3231RegRead(DS3231_REG_CONTROL);
  if (rate & 0b1) // RS1
    control |= RS1;
  else
    control &= ~RS1;
  
  if (rate & 0b10) // RS2
    control |= RS2;
  else
    control &= ~RS2;

  control &= ~INTCN; // Turn on SQW
  ds3231RegWrite(DS3231_REG_CONTROL, control);
}

// 0 -> off, 1 -> on
void ds3231_32kSet(byte value)
{
  byte ctl_sta = ds3231RegRead(DS3231_REG_CTL_STA);
  if (value) // RS1
    ctl_sta |= EN32kHz;
  else
    ctl_sta &= ~EN32kHz;
  
  ds3231RegWrite(DS3231_REG_CTL_STA, ctl_sta);
}

void ds3231StartTempConv()
{
  byte control = ds3231RegRead(DS3231_REG_CONTROL);
  control |= CONV;
  ds3231RegWrite(DS3231_REG_CONTROL, control);
  
  unsigned long start = millis();
  unsigned long stp = start;
  unsigned long init = start;
  
  while(1)
  {
    byte ctl_sta = ds3231RegRead(DS3231_REG_CTL_STA);
    if (ctl_sta & BSY)
    {
      init = millis();
      break;
    }
    // Check timeout
    if (millis() - start > 2500)
      break;
  }
  
  while(1)
  {
    byte ctl_sta = ds3231RegRead(DS3231_REG_CTL_STA);
    if (!(ctl_sta & BSY))
    {
      stp = millis();
      break;
    }
    // Check timeout
    if (millis() - start > 5000)
      break;
  }
  
  Serial.print("Init period, msec: ");
  Serial.println(init - start);
  Serial.print("Conv period, msec: ");
  Serial.println(stp - start);
}
