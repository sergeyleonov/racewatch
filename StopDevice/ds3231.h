#ifndef __DS3231_H__
#define __DS3231_H__

#include "Arduino.h"
#include "Wire.h"

const byte DS3231_I2C_ADDRESS  = 0x68;
const byte DS3231_REG_SECONDS  = 0x00;
const byte DS3231_REG_MINUTES  = 0x01;
const byte DS3231_REG_HOURS    = 0x02;
const byte DS3231_REG_DAY_OF_WEEK = 0x03;
const byte DS3231_REG_DATE     = 0x04;
const byte DS3231_REG_MONTH    = 0x05;
const byte DS3231_REG_YEAR     = 0x06;

const byte DS3231_REG_CONTROL  = 0x0E;
const byte DS3231_REG_CTL_STA  = 0x0F;

const byte DS3231_REG_TEMP_MSB = 0x11;
const byte DS3231_REG_TEMP_LSB = 0x12;

// Control register bits:
const byte A1IE  =        0b1;
const byte A2IE  =       0b10;
const byte INTCN =      0b100;
const byte RS1   =     0b1000;
const byte RS2   =    0b10000;
const byte CONV  =   0b100000;
const byte BBSQW =  0b1000000;
const byte EOSC  = 0b10000000;

// Control/status register bits:
const byte BSY     =  0b100;
const byte EN32kHz = 0b1000;

const byte SQW_1Hz    = 0b00;
const byte SQW_1024Hz = 0b01;
const byte SQW_4096Hz = 0b10;
const byte SQW_8192Hz = 0b11;

struct Time
{
  uint8_t second;
  uint8_t minute;
  uint8_t hour;
  uint8_t dayOfWeek;
  uint8_t date;
  uint8_t month;
  uint8_t year;
};

void ds3231TimeSet(Time &);
void ds3231TimeGet(Time &);
float ds3231TempGet();
void ds3231RateSet(byte);
void ds3231StartTempConv();
void ds3231_32kSet(byte);
byte ds3231RegRead(byte);
void ds3231RegWrite(byte, byte);

#endif //__DS3231_H__
