/*
* Sketch for nRF24L01+ radios, DS3231 clock
*/
#include <SPI.h>
#include "RF24.h"
#include "LowPower.h"
#include "payload.h"
#include "Wire.h"
#include "ds3231.h"

volatile unsigned long clock_ticks = 0;
uint8_t sendings_count = 0;
float vbat = 0.0;
unsigned long speed_time = 0;
unsigned long stop_time = 0;

unsigned long last_measure_time = 0;
unsigned long last_sending_time = 0;
const unsigned long MEASURE_DELAY = 10000;
const unsigned long SEND_DELAY = 1000;

bool radio_number = 1; // Set this radio as radio number 0 or 1
RF24 radio(7, 8); // Set up nRF24L01 radio on SPI bus plus pins 7 & 8
byte addresses[][6] = {"1Node", "2Node"};
float vrefs[] = {1.088, 1.088}; // 1.088V for both boards
float vratio[] = {20.1, 14.38};
bool role = 1; // Defines whether this node is sending or receiving

Payload payload_out;
Payload payload_in;

const byte clockIntPin = 3; // TODO
const byte sensorIntPin = 2; // TODO
const byte speedInPin = 4;
const byte stopInPin = 5;

void setup() {
  initADC();
  Serial.begin(57600);
  Serial.println(F("RF24/examples/GettingStarted"));

  // nRF24 initializing
  radio.begin();

  // Set the PA Level low to prevent power supply related issues since this is a
  // getting_started sketch, and the likelihood of close proximity of the devices. RF24_PA_MAX is default.
  radio.setPALevel(RF24_PA_LOW);
  radio.setRetries(5, 3);
  radio.setPayloadSize(sizeof(Payload));
  //radio.setDataRate(RF24_250KBPS);

  Serial.println(F("*** Starting transmission"));
  radio.openWritingPipe(addresses[1]);
  radio.openReadingPipe(1,addresses[0]);
  radio.stopListening(); // Added

  // DS3231 initializing
  Wire.begin();
  ds3231RateSet(SQW_1024Hz);
  ds3231_32kSet(0);
  
  pinMode(clockIntPin, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(clockIntPin), clockTickInt, FALLING);

  pinMode(sensorIntPin, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(sensorIntPin), sensorInt, FALLING);
}

void loop() {
  unsigned long current_time = millis();
  if (current_time > last_measure_time + MEASURE_DELAY - 1) {
    last_measure_time = current_time;
    vbat = getBateryVoltage();
    //Serial.print(F("Battery, mV: "));
    //Serial.println(payload_out.vbat);
  }
  /****************** Ping Out Role ***************************/
  if (role) {
    if (current_time > last_sending_time + SEND_DELAY - 1) {
      last_sending_time = current_time - (current_time % SEND_DELAY);
      //Serial.flush();
      //LowPower.powerDown(SLEEP_2S, ADC_OFF, BOD_OFF);
      
      //Serial.print(sizeof(Payload));
  
      if (sendings_count == 0) {
        payload_out.answerType = STOP_TIME_ANSWER;
        payload_out.data.time = stop_time;
        Serial.print("Writing stop time = ");
        Serial.print(stop_time);
      }
      else if (sendings_count == 1) {
        payload_out.answerType = CURRENT_TIME_ANSWER;
        payload_out.data.time = clock_ticks;
        Serial.print("Writing current time = ");
        Serial.print(clock_ticks);
      }
      else if (sendings_count == 2) {
        payload_out.answerType = VBAT_ANSWER;
        payload_out.data.vbat = vbat;
        Serial.print("Writing vbat = ");
        Serial.print(vbat);
      }

      long starttime = micros();
      bool result = radio.write(&payload_out, sizeof(Payload));
      long stoptime = micros();
      Serial.print(F(": "));
      Serial.print(stoptime - starttime);      
      Serial.print(F(" us"));
      if (!result) {
        Serial.print(F(": Failed to write"));
      }
      Serial.print("\n");
      sendings_count++;
      if (sendings_count > 2)
        sendings_count = 0;
    }
  }
}

void initADC() {
  // Set the reference to Vref and the measurement to the A0
  // REFS1  REFS0   Voltage Reference Selection 
  // 0      0   AREF, Internal Vref turned off 
  // 0      1   AVcc with external capacitor on AREF pin 
  // 1      0   Reserved 
  // 1      1   Internal 1.1V (ATmega168/328)
  ADMUX = _BV(REFS1) | _BV(REFS0)  | _BV(MUX1);
}

float getBateryVoltage()
{
  // It is better to measure Vref and use the real value: according to the datasheet
  // it could be in [1.0..1.2]V range
  const float nominator = vratio[radio_number] * vrefs[radio_number] / 1023.0; // in Volts
  unsigned short int vcc;
  float result;

  ADCSRA |= _BV(ADSC);  // Start conversion
  while (bit_is_set(ADCSRA, ADSC)); // Measuring
  vcc = ADCL;       // Read the lower byte to lock the high byte
  vcc |= ADCH << 8;
  result =  nominator * (float)vcc;

  return result;
}

float getVcc()
{
  // It is better to measure Vref and use the real value: according to the datasheet
  // it could be in [1.0..1.2]V range
  const float nominator = 1.071 * 1023.0 * 1000.0; // (Vref * 1023.0 * 1000) (in mV). //1.071V for the in-line-pinned board
  unsigned short int vcc;
  float result;
  // Read 1.1V reference against AVcc
  // set the reference to Vcc and the measurement to the internal 1.1V reference
  // REFS1  REFS0   Voltage Reference Selection 
  // 0      0   AREF, Internal Vref turned off 
  // 0      1   AVcc with external capacitor on AREF pin 
  // 1      0   Reserved 
  // 1      1   Internal 1.1V (ATmega168/328)
  ADMUX = _BV(REFS0) | _BV(MUX3) | _BV(MUX2) | _BV(MUX1);
  delay(75);        // Wait for Vref to settle
  ADCSRA |= _BV(ADSC);  // Start conversion
  while (bit_is_set(ADCSRA, ADSC)); // Measuring
  vcc = ADCL;       // Read the lower byte to lock the high byte
  vcc |= ADCH << 8;
  result =  nominator / (float)vcc;

  return result;
}

// Clock interrupt
void clockTickInt() {
  clock_ticks++;
}

// Sensor signal interrupt
void sensorInt() {
  unsigned long cur_time = clock_ticks;
  int state = digitalRead(sensorIntPin);
  /*if (cur_time - speed_time < 1024) // 1024 ticks = 1 second
    return;*/
  if (cur_time - stop_time < 1024) // 1024 ticks = 1 second
    return;
  stop_time = cur_time;
  
  Serial.print("stop_time set = ");
  Serial.print(stop_time);
  Serial.print(", state = ");
  Serial.println(state);
}
