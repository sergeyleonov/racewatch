#ifndef __RF24_CONFIG_H__
#define __RF24_CONFIG_H__

/*** USER DEFINES:  ***/  
//#define FAILURE_HANDLING
//#define SERIAL_DEBUG
//#define MINIMAL

/**********************/
#include "stm32f10x.h"
#include "HardwareSPI.h"
#include <stdio.h> // for printf
#include <string.h> // for memcpy

#define rf24_max(a,b) (a>b?a:b)
#define rf24_min(a,b) (a<b?a:b)

#define _BV(x) (1<<(x))

#ifdef SERIAL_DEBUG
  #define IF_SERIAL_DEBUG(x) ({x;})
#else
  #define IF_SERIAL_DEBUG(x)
#endif

#define pgm_read_byte(addr) (*(const uint8_t *)(addr))

#endif // __RF24_CONFIG_H__
