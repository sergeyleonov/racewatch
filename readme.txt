Race Watch

SYSCLK 72 Mhz
HCLK 72 Mhz
PCLK1 36 Mhz
PCLK2 72 Mhz
ADCCLK 36 Mhz

Pin map:
  //PA0-7		ADC inputs
  //PA1		Led interrupt input
  PA5-7		RF24 SPI
  //PA9		Ext ClockTimer counter input (SQW)
  PA11,12	USB
  PA13,14	SWD
  
  PB0-7		Digits cathodes
  PB1		Board led //, switched by PC13
  //PB3-5		PCD8544 control
  PB8		Button
  PB9		USB disconnect
  //PB10,11	DS3231 I2C
  PB10,11	RF24 control
  PB12-15	Digits anodes
  //PB13,15	PCD8544 SPI
  
  PC13		Led interrupt input
  //PC14,15	mcu_i2c control