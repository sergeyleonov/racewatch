#ifndef __SYSTEM_MODE_H
#define __SYSTEM_MODE_H

#include "stm32f10x.h"

enum SystemModes
{
	SS_Init,
	SS_Started,
	SS_Stopped
};

#endif
