/**
  ******************************************************************************
  * @file    hw_config.c
  * @author  MCD Application Team
  * @version V4.0.0
  * @date    21-January-2013
  * @brief   Hardware Configuration & Setup
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2013 STMicroelectronics</center></h2>
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */

#include "platform_config.h"
#include "usb_type.h"
#include "hw_config.h"
#include "usb_lib.h"
#include "usb_desc.h"
#include "usb_pwr.h"

#include "delay.h"

//__IO uint8_t DataReady = 0;
//__IO uint8_t PrevXferComplete = 1;

//static void IntToUnicode (uint32_t value , uint8_t *pbuf , uint8_t len);

/*******************************************************************************
* Function Name  : Set_USB_Hardware
* Description    : Configures Main system clocks & power.
*******************************************************************************/
void Set_USB_Hardware(void)
{
  EXTI_InitTypeDef EXTI_InitStructure;
  /*!< At this stage the microcontroller clock setting is already configured,
       this is done through SystemInit() function which is called from startup
       file (startup_stm32xxx.s) before to branch to application main.
       To reconfigure the default setting of SystemInit() function, refer to
       system_stm32xxx.c file
     */
  GPIO_InitTypeDef GPIO_InitStructure;
    
  /* Enable the PWR clock */
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR, ENABLE);
    
   /*Set PA11,12 as IN - USB_DM,DP*/
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
  GPIO_StructInit(&GPIO_InitStructure); // All | 2MHz | IN_FLOATING
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_11 | GPIO_Pin_12;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_OD; // TODO: check!
  GPIO_Init(GPIOA, &GPIO_InitStructure);
      
  /*SET PA11,12 for USB: USB_DM,DP*/
  //GPIO_PinAFConfig(GPIOA, GPIO_PinSource11, GPIO_AF_14);
  //GPIO_PinAFConfig(GPIOA, GPIO_PinSource12, GPIO_AF_14);

#if defined(USB_USE_EXTERNAL_PULLUP)
  /* Enable the USB disconnect GPIO clock */
  RCC_AHBPeriphClockCmd(RCC_APB2Periph_GPIO_DISCONNECT, ENABLE);

  /* USB_DISCONNECT used as USB pull-up */
  GPIO_InitStructure.GPIO_Pin = USB_DISCONNECT_PIN;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
  GPIO_Init(USB_DISCONNECT, &GPIO_InitStructure);
  
  //delay_us(5); // TODO:
  
  GPIO_SetBits(USB_DISCONNECT, USB_DISCONNECT_PIN);
#endif /* USB_USE_EXTERNAL_PULLUP */

  /* Configure the EXTI line 18 connected internally to the USB IP ************/
  EXTI_ClearITPendingBit(EXTI_Line18);
  EXTI_InitStructure.EXTI_Line = EXTI_Line18;
  EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising;
  EXTI_InitStructure.EXTI_LineCmd = ENABLE;
  EXTI_Init(&EXTI_InitStructure);
}

void InitLedsButtons(void)
{
  GPIO_InitTypeDef GPIO_InitStructure;
  /* Buttons */
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIO_BUTTON, ENABLE);
  GPIO_StructInit(&GPIO_InitStructure); // All | 2MHz | IN_FLOATING
  GPIO_InitStructure.GPIO_Pin = BTN1_PIN;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
  GPIO_Init(BTN1_PORT, &GPIO_InitStructure);

  /* LEDs */
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIO_LED, ENABLE);
  GPIO_InitStructure.GPIO_Pin = LED1A_PIN;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
  GPIO_Init(LED_PORT, &GPIO_InitStructure);
}

/*******************************************************************************
* Function Name  : Set_USB_Clock
* Description    : Configures USB Clock input (48MHz).
*******************************************************************************/
void Set_USB_Clock(void)
{
#if defined(STM32L1XX_MD) || defined(STM32L1XX_HD)|| defined(STM32L1XX_MD_PLUS)
  /* Enable USB clock */
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_USB, ENABLE);

#else
  /* Select USBCLK source */
  RCC_USBCLKConfig(RCC_USBCLKSource_PLLCLK_1Div5);

  /* Enable the USB clock */
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_USB, ENABLE);
#endif /* STM32L1XX_XD */

}

void USB_Config(void)
{
  Set_USB_Hardware();
  Set_USB_Clock();
  USB_Interrupts_Config();
  
  //USB_Init(); // USB-setup
  //while (bDeviceState != CONFIGURED) {} // USB-setup
}

/*******************************************************************************
* Function Name  : Enter_LowPowerMode.
* Description    : Power-off system clocks and power while entering suspend mode.
*******************************************************************************/
void Enter_LowPowerMode(void)
{
  /* Set the device state to suspend */
  //bDeviceState = SUSPENDED; // USB-setup

  /* Clear EXTI Line18 pending bit */
//  EXTI_ClearITPendingBit(KEY_BUTTON_EXTI_LINE);

  /* Request to enter STOP mode with regulator in low power mode */
  PWR_EnterSTOPMode(PWR_Regulator_LowPower, PWR_STOPEntry_WFI);
}

/*******************************************************************************
* Function Name  : Leave_LowPowerMode.
* Description    : Restores system clocks and power while exiting suspend mode.
*******************************************************************************/
void Leave_LowPowerMode(void)
{
  // USB-setup
  /*DEVICE_INFO *pInfo = &Device_Info;

  // Set the device state to the correct state
  if (pInfo->Current_Configuration != 0)
  {
    // Device configured
    bDeviceState = CONFIGURED;
  }
  else
  {
    bDeviceState = ATTACHED;
  }*/

  /*Enable SystemCoreClock*/
  SystemInit();
}

/*******************************************************************************
* Function Name  : USB_Interrupts_Config.
* Description    : Configures the USB interrupts.
*******************************************************************************/
void USB_Interrupts_Config(void)
{
  NVIC_InitTypeDef NVIC_InitStructure;

  /* 2 bit for pre-emption priority, 2 bits for subpriority */
  NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);

  /* Enable the USB interrupt */
  NVIC_InitStructure.NVIC_IRQChannel = USB_LP_CAN1_RX0_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 2;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);

  /* Enable the USB Wake-up interrupt */
  NVIC_InitStructure.NVIC_IRQChannel = USBWakeUp_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
  NVIC_Init(&NVIC_InitStructure);
}

/*******************************************************************************
* Function Name  : USB_Cable_Config.
* Description    : Software Connection/Disconnection of USB Cable.
* Input          : NewState: new state.
*******************************************************************************/
void USB_Cable_Config (FunctionalState NewState)
{
  if (NewState != DISABLE)
  {
    GPIO_ResetBits(USB_DISCONNECT, USB_DISCONNECT_PIN);
  }
  else
  {
    GPIO_SetBits(USB_DISCONNECT, USB_DISCONNECT_PIN);
  }
}

/*******************************************************************************
* Function Name : RHIDCheckState.
* Description   : Decodes the RHID state.
* Return value  : The state value.
*******************************************************************************/
uint8_t Buffer[RPT4_COUNT+1];

uint8_t RHIDCheckState(void)
{
  // Switch status LED
  GPIOB->ODR ^= GPIO_Pin_1;
  
  Buffer[0] = 4;
  Buffer[1] = GPIO_ReadInputDataBit(BTN1_PORT, BTN1_PIN);
  Buffer[2] = 0; // Button 2 is unused //btn2 = GPIO_ReadInputDataBit(BTN2_PORT, BTN2_PIN);
  Buffer[3] = 0; // (GPIO->IDR & GPIO_Pin_1) == GPIO_Pin_1;
  //Buffer[4] is set in EP1_OUT_Callback function

  /* Reset the control token to inform upper layer that a transfer is ongoing */
  //PrevXferComplete = 0;

  /* Copy info in ENDP1 Tx Packet Memory Area*/
  //USB_SIL_Write(EP1_IN, Buffer, RPT4_COUNT+1); // USB-setup
  /* Enable endpoint for transmission */
  //SetEPTxValid(ENDP1); // USB-setup
  
  return 0;
}

/*******************************************************************************
* Function Name : RHID_Send.
* Description   : prepares buffer to be sent containing event infos.
* Input         : Keys: keys received from terminal.
*******************************************************************************/
/*void RHID_Send(uint8_t report, uint8_t state)
{
  uint8_t Buffer[2] = {0, 0};

  Buffer[0] = report;
  Buffer[1] = state;

  // Reset the control token to inform upper layer that a transfer is ongoing
  PrevXferComplete = 0;

  // Copy buttons data in ENDP1 Tx Packet Memory Area
  USB_SIL_Write(EP1_IN, Buffer, 2);
  // Enable endpoint for transmission 
  SetEPTxValid(ENDP1);
}*/

/*******************************************************************************
* Function Name  : Get_SerialNum.
* Description    : Create the serial number string descriptor.
*******************************************************************************/
void Get_SerialNum(void)
{
  //uint32_t Device_Serial0, Device_Serial1, Device_Serial2;

  //Device_Serial0 = *(uint32_t*)ID1;
  //Device_Serial1 = *(uint32_t*)ID2;
  //Device_Serial2 = *(uint32_t*)ID3;

  //Device_Serial0 += Device_Serial2;

  //if (Device_Serial0 != 0)
  {
    //IntToUnicode (Device_Serial0, &RHID_StringSerial[2] , 8); // USB-setup
    //IntToUnicode (Device_Serial1, &RHID_StringSerial[18], 4); // USB-setup
  }
}

/*******************************************************************************
* Function Name  : HexToChar.
* Description    : Convert Hex 32Bits value into char.
*******************************************************************************/
/*static void IntToUnicode (uint32_t value , uint8_t *pbuf , uint8_t len)
{
  uint8_t idx = 0;

  for( idx = 0 ; idx < len ; idx ++)
  {
    if( ((value >> 28)) < 0xA )
    {
      pbuf[ 2* idx] = (value >> 28) + '0';
    }
    else
    {
      pbuf[2* idx] = (value >> 28) + 'A' - 10;
    }

    value = value << 4;

    pbuf[ 2* idx + 1] = 0;
  }
}*/

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
