#include "led_display.h"
#include "stm32f10x_tim.h"
#include "delay.h"

#define SYMBOLS_NUMBER 4
#define ANODES_WILDCARD 0xF000

__IO uint32_t CurrentSymbol = 0;
static uint8_t Symbols[SYMBOLS_NUMBER] = {0, 0, 0, 0};

//0123456789 _-`abcdefg.
static uint8_t CodePage[] = {0x5F, 0x0C, 0x9B, 0x9E, 0xCC, 
					0xD6, 0xD7, 0x1C, 0xDF, 0xDE, 
					0x00, 0x02, 0x80, 0x10, 0x10, 
					0x08, 0x04, 0x02, 0x01, 0x40, 
					0x80, 0x20};

void ledDisplay_Reset()
{
	uint32_t idx;
	for (idx = 0; idx < SYMBOLS_NUMBER; idx++)
		Symbols[idx] = 0;
}

void ledDisplay_SetDot()
{
	Symbols[1] &= 0x20;
}

void ledDisplay_RemoveDot()
{
	Symbols[1] &= ~0x20;
}

uint32_t getDigitCode(uint32_t character) {
	if (character == 0)
		character = 10;
	else if (character == '_')
		character = 11;
	else if (character == '-')
		character = 12;
	else if (character == '`')
		character = 13;
	else if (character == '.')
		character = 21;
	else if (character < '0')
		character = 11;
	else if (character <= '9')
		character = character - '0';
	else if (character >= 'a' && character <= 'g') // a..g
		character = character - 'a' + 14;
	else
		character = 11;
	
	return character;
}

void ledDisplay_SetChar(char character, uint32_t position)
{
	uint32_t digit = getDigitCode(character);
	Symbols[position] = CodePage[digit]; // ASCII code
}

void ledDisplay_SetText(char characters[])
{
	uint32_t idx;
	for (idx = 0; idx < SYMBOLS_NUMBER; idx++) {
		uint32_t digit = characters[idx];
		digit = getDigitCode(digit);
		Symbols[idx] = CodePage[digit]; // ASCII code
	}
}

void ledDisplay_SetMsecs(uint32_t msecs) {
	char buffer[4];
	uint32_t remains = msecs / 10; // We can display only 1/100 of a sec
	buffer[3] = (remains % 10) + '0';
	remains /= 10;
	buffer[2] = (remains % 10) + '0';
	remains /= 10;
	buffer[1] = (remains % 10) + '0';
	remains /= 10;
	buffer[0] = (remains % 10) + '0';
	if (buffer[0] == '0')
		buffer[0] = 0; // Remove trailing zero
	ledDisplay_SetText(buffer);
	Symbols[1] |= 0x20; // Set dot
}

void ledDisplay_SetSpeed(uint32_t speed) {
	char buffer[4];
	uint32_t remains = speed;
	
	buffer[3] = (remains % 10) + '0';
	remains /= 10;
	buffer[2] = (remains % 10) + '0';
	remains /= 10;
	buffer[1] = (remains % 10) + '0';
	remains /= 10;
	buffer[0] = (remains % 10) + '0';
	if (buffer[0] == '0')
		buffer[0] = 0; // Remove trailing zero
	ledDisplay_SetText(buffer);
}

void ledDisplay_Init()
{
	GPIO_InitTypeDef GPIO_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;
	TIM_TimeBaseInitTypeDef TIM_InitStructure;
	
	// Disable the Serial Wire Jtag Debug Port SWJ-DP
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);
	GPIO_PinRemapConfig(GPIO_Remap_SWJ_NoJTRST, ENABLE);
	GPIO_PinRemapConfig(GPIO_Remap_SWJ_JTAGDisable, ENABLE);
	//GPIO_PinRemapConfig(GPIO_Remap_SWJ_Disable, ENABLE);
	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
	GPIO_StructInit(&GPIO_InitStructure); // All | 2MHz | IN_FLOATING
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3 | GPIO_Pin_4 | GPIO_Pin_5 | GPIO_Pin_6 | GPIO_Pin_7;
	//GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_Init(GPIOB, &GPIO_InitStructure);

	//RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
	GPIO_StructInit(&GPIO_InitStructure); // All | 2MHz | IN_FLOATING
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_12 | GPIO_Pin_13 | GPIO_Pin_14 | GPIO_Pin_15;
	//GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_Init(GPIOB, &GPIO_InitStructure);
	
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);

	TIM_TimeBaseStructInit(&TIM_InitStructure);
	TIM_InitStructure.TIM_Prescaler = 720; // 72 MHz / 720 = 100 kHz
	TIM_InitStructure.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_InitStructure.TIM_Period = 200; // 100 kHz / 200 = 800 Hz => each digit refresh rate is 200 Hz
	TIM_InitStructure.TIM_ClockDivision = TIM_CKD_DIV1;
	TIM_InitStructure.TIM_RepetitionCounter = 0;
	TIM_TimeBaseInit(TIM2, &TIM_InitStructure);
	TIM_Cmd(TIM2, ENABLE);
	
	TIM_ITConfig(TIM2, TIM_IT_Update, ENABLE);

	NVIC_InitStructure.NVIC_IRQChannel = TIM2_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	
	/*RCC->APB2ENR |= RCC_APB2ENR_TIM2EN;			// enable clock for TIM2

	TIM2->PSC = 72;	// set prescaler // TODO
	TIM2->ARR = 1000;	// set auto-reload
	TIM2->RCR = 0;								// set repetition counter

	TIM2->CR1 = 0;								// reset command register 1
	TIM2->CR2 = 0;								// reset command register 2
	TIM2->DIER = 1;								// enable interrupt
	NVIC->ISER[0] |= (1 << (TIM2_UP_IRQChannel & 0x1F));	// enable interrupt
	TIM2->CR1 |= TIM_CR1_CEN;					// enable timer*/
}

void TIM2_IRQHandler()
{
	// For acceleration: if ((TIM2->SR & 0x0001) != 0) // check interrupt source
	if (TIM_GetITStatus(TIM2, TIM_IT_Update) != RESET)
	{
		// For acceleration: TIM2->SR &= ~(1 << 0);	// clear UIF flag
		TIM_ClearITPendingBit(TIM2, TIM_IT_Update);
		
		GPIOB->BRR = ANODES_WILDCARD;		// Turn off anodes
		delay_us(75); // TODO: enable dead time
		GPIOB->BRR = 0xFF;		// Reset zeros
		GPIOB->BSRR = Symbols[CurrentSymbol];		// Set ones
		
		GPIOB->BSRR = (1 << (CurrentSymbol + 12));	// Turn on anodes
		
		CurrentSymbol++;
		if (CurrentSymbol >= SYMBOLS_NUMBER)
			CurrentSymbol = 0;
	}
}
