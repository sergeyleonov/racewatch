#ifndef __DELAY_H
#define __DELAY_H

#include "stm32f10x.h"
#include "stm32f10x_tim.h"

void InitSysTick(void);
void InitDelayTimer(void);
void delay_us(uint16_t);
void delay_ms(uint32_t);
void delay_MeasureStart(void);
uint16_t delay_MeasureGet(void);

#endif
