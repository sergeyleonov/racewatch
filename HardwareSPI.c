/******************************************************************************
 * The MIT License
 *
 * Copyright (c) 2010 Perry Hung.
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *****************************************************************************/

/**
 * @author Marti Bolivar <mbolivar@leaflabs.com>
 * @brief Wirish SPI implementation.
 */

#include "HardwareSPI.h"

/*
 * Set up/tear down
 */

void HardwareSPI_begin() {
	GPIO_InitTypeDef GPIO_InitStructure;
	SPI_InitTypeDef SPI_InitStructure;
	
	RCC_APB2PeriphClockCmd(RF24_APB2_GPIO, ENABLE); // тактирование GPIO
	RCC_APB2PeriphClockCmd(RF24_SPI_APB2_GPIO, ENABLE); // тактирование GPIO
	//RCC_AHBPeriphClockCmd(RF24_AHB_GPIO, ENABLE); // тактирование GPIO // STM32F3
	//RCC_AHBPeriphClockCmd(RF24_SPI_AHB_GPIO, ENABLE); // тактирование GPIO // STM32F3
	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE); // STM32F1
	GPIO_PinRemapConfig(GPIO_Remap_SWJ_JTAGDisable, ENABLE); // STM32F1
	//GPIO_PinAFConfig(GPIOB, GPIO_PinSource13, GPIO_AF_5); // SPI2_CLK (Pin_13) // STM32F3
	//GPIO_PinAFConfig(GPIOB, GPIO_PinSource14, GPIO_AF_5); // SPI2_MISO (Pin_14) // STM32F3
	//GPIO_PinAFConfig(GPIOB, GPIO_PinSource15, GPIO_AF_5); // SPI2_MOSI (Pin_15) // STM32F3
	
	// тактирование SPI
	RCC_APB2PeriphClockCmd(RF24_RCC_Periph, ENABLE);
	//RCC_APB1PeriphClockCmd(RF24_RCC_Periph, ENABLE); // STM32F3
	
	//STM32F1
	GPIO_StructInit(&GPIO_InitStructure); // All | 2MHz | IN_FLOATING
	GPIO_InitStructure.GPIO_Pin			= (1<<RF24_SCK_PIN) | (1<<RF24_MOSI_PIN);
	GPIO_InitStructure.GPIO_Speed		= GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode		= GPIO_Mode_AF_PP;
	GPIO_Init(RF24_SPI_GPIO, &GPIO_InitStructure);
	
	GPIO_InitStructure.GPIO_Pin			= (1<<RF24_MISO_PIN);
	GPIO_InitStructure.GPIO_Mode		= GPIO_Mode_IPU;
	GPIO_Init(RF24_SPI_GPIO, &GPIO_InitStructure);

	/* STM32F3
	GPIO_StructInit(&GPIO_InitStructure); // All | 2MHz | IN_FLOATING
	GPIO_InitStructure.GPIO_Pin			= (1<<RF24_SCK_PIN) | (1<<RF24_MOSI_PIN) | (1<<RF24_MISO_PIN);
	GPIO_InitStructure.GPIO_Speed		= GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode		= GPIO_Mode_AF_PP; 
	GPIO_InitStructure.GPIO_Mode		= GPIO_Mode_AF; // STM32F3
	//GPIO_InitStructure.GPIO_OType		= GPIO_OType_PP; // STM32F3
	
	
	GPIO_Init(RF24_SPI_GPIO, &GPIO_InitStructure);*/

	SPI_I2S_DeInit(RF24_SPIx); // сбрасываем настройки SPI перед заданием конфигурации
	SPI_InitStructure.SPI_Mode		= SPI_Mode_Master;
	SPI_InitStructure.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
	SPI_InitStructure.SPI_DataSize	= SPI_DataSize_8b;
	SPI_InitStructure.SPI_CPOL		= SPI_CPOL_Low;
	SPI_InitStructure.SPI_CPHA		= SPI_CPHA_1Edge;
	SPI_InitStructure.SPI_NSS		= SPI_NSS_Soft;
	SPI_InitStructure.SPI_BaudRatePrescaler = RF24_SPI_Prescaler;
	SPI_InitStructure.SPI_FirstBit	= SPI_FirstBit_MSB;

	SPI_Init(RF24_SPIx, &SPI_InitStructure);
	SPI_Cmd(RF24_SPIx, ENABLE);
	
	// Поскольку сигнал NSS контролируется программно, установим его в единицу
	// Если сбросить его в ноль, то наш SPI модуль подумает, что
	// у нас мультимастерная топология и его лишили полномочий мастера.
	SPI_NSSInternalSoftwareConfig(RF24_SPIx, SPI_NSSInternalSoft_Set);

	GPIO_StructInit(&GPIO_InitStructure); // All | 2MHz | IN_FLOATING
	GPIO_InitStructure.GPIO_Pin 	= (1<<RF24_CS_PIN) | (1<<RF24_CE_PIN);
	GPIO_InitStructure.GPIO_Mode	= GPIO_Mode_Out_PP;
	//GPIO_InitStructure.GPIO_PuPd 	= GPIO_PuPd_UP; // STM32F3
	//GPIO_InitStructure.GPIO_Mode	= GPIO_Mode_OUT; // STM32F3
	//GPIO_InitStructure.GPIO_OType	= GPIO_OType_OD; // STM32F3
	GPIO_InitStructure.GPIO_Speed	= GPIO_Speed_50MHz;
	GPIO_Init(RF24_GPIO, &GPIO_InitStructure);
	
	//SPI_RxFIFOThresholdConfig(SPI2, SPI_RxFIFOThreshold_QF); // STM32F3
}

/*
 * I/O
 */

uint8_t HardwareSPI_transfer(uint8_t byte) {
  uint32_t timeout = RF_FLAG_TIMEOUT;
  uint8_t answer;

  /* Loop while DR register in not empty */
  while (SPI_I2S_GetFlagStatus(RF24_SPIx, SPI_I2S_FLAG_TXE) == RESET)
  {
    if((timeout--) == 0)
      return 0;
  }
  SPI_I2S_SendData(RF24_SPIx, byte);
  //SPI_SendData8(RF24_SPIx, byte); // STM32F3

  timeout = RF_FLAG_TIMEOUT;
  while (SPI_I2S_GetFlagStatus(RF24_SPIx, SPI_I2S_FLAG_RXNE) == RESET)
  {
    if((timeout--) == 0)
      return 0;
  }
  answer = (uint8_t)SPI_I2S_ReceiveData(RF24_SPIx);
  //answer = (uint8_t)SPI_ReceiveData8(RF24_SPIx); // STM32F3
  return answer;
}
