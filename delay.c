#include "delay.h"

__IO uint32_t DelayCounter = 0;
__IO uint32_t SysTickCounter = 0;		// System Tick counter

// This routine uses SysTick timer
void delay_ms(uint32_t delayValue)
{
	DelayCounter = delayValue + 1;
	while (DelayCounter != 0);
}

void InitSysTick()
{
	RCC_ClocksTypeDef RCC_Clocks;
	RCC_GetClocksFreq(&RCC_Clocks);
	SysTick_Config(RCC_Clocks.HCLK_Frequency / 1000); // 1/1000 of a second per tick
}

void InitDelayTimer()
{
	TIM_TimeBaseInitTypeDef TIM_InitStructure;
	
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);
	
	TIM_TimeBaseStructInit(&TIM_InitStructure);
	TIM_InitStructure.TIM_Prescaler = 72 + 1;	// 72 at 72 MHz -> once per 1 us
	TIM_InitStructure.TIM_Period = 1000;		// 1000 -> once per 1 ms
	TIM_TimeBaseInit(TIM3, &TIM_InitStructure);
	
	TIM_SelectOnePulseMode(TIM3, TIM_OPMode_Single);
	
	//NVIC_EnableIRQ(TIM3_DAC_IRQn);
	//TIM_ITConfig(TIM3, TIM_DIER_UIE, ENABLE);
}

/*void TIM3_DAC_IRQHandler(void)
{
	if (TIM_GetITStatus(TIM3, TIM_IT_Update) != RESET)
	{
		TIM_ClearITPendingBit(TIM3, TIM_IT_Update);
		//timer_busy = 0;
		//TIM_Cmd(TIM3, DISABLE);
	}
	
}*/

// Delay using a timer
void delay_us(uint16_t delayValue)
{
	TIM3->ARR = delayValue;
	TIM3->CR1 |= TIM_CR1_CEN;
	while ((TIM3->CR1 & TIM_CR1_CEN) != 0);
}

void delay_MeasureStart()
{
	TIM3->CR1 &= !TIM_CR1_CEN;
	TIM3->CNT = 0xFFFF;
	TIM3->ARR = 0xFFFF;
	TIM3->CR1 |= TIM_CR1_CEN;
}

uint16_t delay_MeasureGet()
{
	uint16_t result;
	result = TIM3->CNT;
	TIM3->CR1 &= !TIM_CR1_CEN;
	return result;
}
