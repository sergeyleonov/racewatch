#ifndef __LED_DISPLAY_H
#define __LED_DISPLAY_H

#include "stm32f10x.h"

void ledDisplay_Init(void);
void ledDisplay_Reset(void);
void ledDisplay_SetChar(char, uint32_t);
void ledDisplay_SetText(char[]);
void ledDisplay_SetMsecs(uint32_t);
void ledDisplay_SetSpeed(uint32_t);
void ledDisplay_SetDot(void);
void ledDisplay_RemoveDot(void);

#endif
